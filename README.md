This project contains a Python class demonstrating how a discrete-event
simulation program can be built step-by-step using function chaining.

The `Trajectory` class defines trajectory objects that can be modified
using a

    traj = traj.<block>(*args)

structure, where `<block>` represents simulation blocks such as
`seize()`, `delay()`, and `release()`.  Delayed evaluation of
expression strings are implemented to allow simulation behavior
to depend on environment variables and entity attributes. These
delayed expression strings can be defined using the format:

    D(<expression_string>,<return_type>)


The `generator()` function creates a generator which feeds entities
to the simulation environment, each of which follows the specified
trajectory.

A number of functions are also provided as shorthand for functions
in the built-in `random` module of Python.
