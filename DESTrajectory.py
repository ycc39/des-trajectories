"""Defines a Trajectory class for describing entity flow in a
discrete-event simulation model.  Trajectories are implemented using
Python generators and can be built programmatically via function
chaining, as demonstrated in the example at the end of this file.

Logging is enabled, using a special format string where all
occurrences of ${foo} and @{bar} are replaced by values of the environment variable
`foo` and the entity attribute `bar`, respectively.

All generated entities will have a default `name` attribute set to
the generator's name appended with an increasing integer, e.g.
'Job.0', 'Job.1', etc.

A more mature implementation of discrete-event simulation trajectories
can be found in the R package simmer: see
https://r-simmer.org/reference/trajectory.html


**Possible to-dos:**

- Build a proper parser for delayed expressions instead of using fyeah.
  Currently, only types that can be initialized with a string are supported.
- Allow delayed expressions for **all** trajectory block parameters.
- Advanced trajectory blocks e.g. batch() and separate() from simmer
- Add set_capacity() for changing resource capacity dynamically
  (How to deal with setting capacity < current usage?)
- Looping, see rollback() in simmer
- Seize/release a **set** of resources (wait until all resources in set are available)
- Automatic resource monitors
- Proper resource tracking to prevent illegal release()'s
"""

import random
import textwrap
from typing import Any, Sequence, Type, TypeVar

from fyeah import f  # WARNING: possibility of code injection, should replace with purpose-built parser
import simpy


class Environment(simpy.Environment):
    """Extends simpy.Environment to include additional data fields."""
    resources: dict[str, simpy.Container]
    globals: dict[str, Any]


class D:
    """Delayed-evaluation string."""
    def __init__(self, _str: str, _type: Type):  # _type must support __init__(self, arg: str)
        self.str = _str
        self.type = _type


UNIF = random.uniform  # UNIF(lo,hi)
TRI = random.triangular  # TRI(lo,hi,mode)
EXP = random.expovariate  # EXP(rate)
GAMM = random.gammavariate  # GAMM(a, b) -- mean = a*b, var = a * b**2
NORM = random.normalvariate  # NORM(mean, st_dev)
LN = random.lognormvariate  # LN(mu, sigma) ~ EXP(NORM(mu, sigma))
WEIB = random.weibullvariate  # WEIB(a,b) -- E[X**n] = a**n * gamma(1+n/b)

TTrajectory = TypeVar('TTrajectory', bound='Trajectory')


class Trajectory:
    """Defines a trajectory an entity will take within a simulation model."""

    def __init__(self, env: Environment):
        def fn(_: dict):
            yield from ()

        self.fn = fn
        self.env = env
        self.strs = []

    def __str__(self):
        if len(self.strs) == 0:
            return "<Empty trajectory>"
        else:
            ret = "Trajectory\n"
            for i, s in enumerate(self.strs):
                ret += textwrap.indent(f'{i}: {s}\n', '   ')
            return ret

    def set_global(self, key: str, expr):
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            val = expr.type(f(expr.str)) if isinstance(expr, D) else expr
            self.env.globals[key] = val

        self.fn = new_fn
        self.strs.append(f'Set global: {key} = {expr.str if isinstance(expr, D) else expr}')
        return self

    def set_attribute(self, key: str, expr):
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            val = expr.type(f(expr.str)) if isinstance(expr, D) else expr
            attrs[key] = val

        self.fn = new_fn
        self.strs.append(f'Set attribute: {key} = {expr.str if isinstance(expr, D) else expr}')
        return self

    def log(self, expr):
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            val = expr.type(f(expr.str)) if isinstance(expr, D) else expr
            print(val)

        self.fn = new_fn
        self.strs.append(f'Log: {expr.str if isinstance(expr, D) else expr}')
        return self

    def seize(self, r_str: str, amount_expr):
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            amount = amount_expr.type(f(amount_expr.str)) if isinstance(amount_expr, D) else amount_expr
            yield self.env.resources[r_str].get(amount)

        self.fn = new_fn
        self.strs.append(f'Get: {r_str}, amount = {amount_expr.str if isinstance(amount_expr, D) else amount_expr}')
        return self

    def delay(self, t_expr):
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            t = t_expr.type(f(t_expr.str)) if isinstance(t_expr, D) else t_expr
            yield self.env.timeout(t)

        self.fn = new_fn
        self.strs.append(f'Delay: {t_expr.str if isinstance(t_expr, D) else t_expr}')
        return self

    def release(self, r_str: str, amount_expr):
        # Note: missing check to see if resource(simpy.Container) was seized
        #       previously within the same trajectory
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            amount = amount_expr.type(f(amount_expr.str)) if isinstance(amount_expr, D) else amount_expr
            yield self.env.resources[r_str].put(amount)

        self.fn = new_fn
        self.strs.append(f'Put: {r_str}, amount = {amount_expr.str if isinstance(amount_expr, D) else amount_expr}')
        return self

    def branch(
            self,
            branch_expr,
            branches: Sequence[TTrajectory]
    ):
        old_fn = self.fn

        def new_fn(attrs: dict):
            env = self.env  # give access of env to the f() call
            yield from old_fn(attrs)
            selected_branch = branch_expr.type(f(branch_expr.str)) if isinstance(branch_expr, D) else branch_expr
            yield from branches[selected_branch].fn(attrs)

        self.fn = new_fn

        s = f'Branch: \n    Select:{branch_expr.str if isinstance(branch_expr, D) else branch_expr}\n'
        for i, br in enumerate(branches):
            s += textwrap.indent(f'{i}: ' + str(br), '    ')
        self.strs.append(s)
        return self


def generator(
        name: str,
        env: Environment,
        inter_arr_expr,
        traj: Trajectory,
        max_n: int | float  # float added to type hint to enable infinite arrivals
):
    n = 0
    while n < max_n:
        attrs = dict(name=f'{name}.{n}')
        if isinstance(inter_arr_expr, D):
            inter_arr = inter_arr_expr.type(f(inter_arr_expr.str))
        else:
            inter_arr = inter_arr_expr
        yield env.timeout(inter_arr)
        env.process(traj.fn(attrs))
        n += 1
