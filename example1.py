import simpy
from DESTrajectory import Environment, Trajectory, generator, D


env1 = Environment()
env1.resources = {'res0': simpy.Container(env1, 1, 1), 'res1': simpy.Container(env1, 1, 1)}
env1.globals = {'n_arr': 0}

# Note that we always use env in the format strings instead of env1
trajectory1 = (
    Trajectory(env=env1)
    .set_attribute('arr_id', D("{env.globals['n_arr']}", int))
    .set_global('n_arr', D("{env.globals['n_arr'] + 1}", int))
    .branch(
        D("{attrs['arr_id'] % 2}", int),  # alternate branches by arr_id
        [
            (
                Trajectory(env=env1)
                .log(D("Entity {attrs['name']}: Chose branch 0, t = {env.now}", str))
                .seize('res0', 1)
                .delay(1.0)
                .release('res0', 1)
            ),
            (
                Trajectory(env=env1)
                .log(D("Entity {attrs['name']}: Chose branch 1, t = {env.now}", str))
                .seize('res1', 1)
                .delay(1.0)
                .release('res1', 1)
            )
        ]
    )
    .log(D("Entity {attrs['name']}: End of branch, t = {env.now}", str))
)
print(trajectory1)

generator1 = generator('Job', env1, 2, trajectory1, max_n=10)
env1.process(generator1)
env1.run()
