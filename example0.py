import random

import simpy
from DESTrajectory import Environment, Trajectory, generator, D


env0 = Environment()
env0.resources = {'res0': simpy.Container(env0, 2, 2)}
env0.globals = {}

# Note that we always use env in the format strings instead of env0
trajectory0 = (
    Trajectory(env=env0)
    .log(D("Entity {attrs['name']}: arrived at time {env.now}, wait for res0", str))

    .seize('res0', 1)
    .log(D("Entity {attrs['name']}: start service (res0) at time {env.now}", str))

    .delay(D("{EXP(1)}", float))

    .release('res0', 1)
    .log(D("Entity {attrs['name']}: end service (res0) at time {env.now}", str))
)

print(trajectory0)

generator0 = generator('Job', env0, D("{EXP(7)}", float), trajectory0, max_n=10)
env0.process(generator0)
random.seed(123456)
env0.run()
